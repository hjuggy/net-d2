﻿/*
 * Изучите код данного приложения для расчета суммы целых чисел от 0 до N, а затем
 * измените код приложения таким образом, чтобы выполнялись следующие требования:
 * 1. Расчет должен производиться асинхронно.
 * 2. N задается пользователем из консоли. Пользователь вправе внести новую границу в процессе вычислений,
 * что должно привести к перезапуску расчета.
 * 3. При перезапуске расчета приложение должно продолжить работу без каких-либо сбоев.
 */

using System;
using System.Threading;
using System.Threading.Tasks;

namespace AsyncAwait.Task1.CancellationTokens
{
    class Program
    {
        static CancellationTokenSource source;


        public static async Task Main(string[] args)
        {
            Console.WriteLine("Calculating the sum of integers from 0 to N.");
            Console.WriteLine("Use 'q' key to exit...");
            Console.WriteLine();


            Console.WriteLine("Enter N: ");
            string input = Console.ReadLine();

            while (input.Trim().ToUpper() != "Q")
            {
                try
                {
                    if (int.TryParse(input, out int n))
                    {
                        source = new CancellationTokenSource();
                        var cancellationToken = source.Token;
                        _ = Task.Run(() => { _ = CalculateSum(n, cancellationToken); });

                        //await CalculateSum(n, cancellationToken);
                    }
                    else
                    {
                        Console.WriteLine($"Invalid integer: '{input}'. Please try again.");
                        Console.WriteLine("Enter N: ");
                    }

                    input = Console.ReadLine();
                    source.Cancel();
                }
                finally
                {
                    source.Dispose();
                }
            }

            Console.WriteLine("Press any key to continue");
            Console.ReadLine();
        }

        private static async Task CalculateSum(int n, CancellationToken cancellationToken)
        {
            Console.WriteLine($"The task for {n} started... Enter N to cancel the request:");

            var sum = await Calculator.Calculate(n, cancellationToken);

            Console.WriteLine($"Sum for {n} = {sum}.");
            Console.WriteLine();
            Console.WriteLine("Enter N: ");
        }
    }
}