﻿using System.Threading.Tasks;

namespace AsyncAwait.Task2.CodeReviewChallenge.Services
{
    public class PrivacyDataService : IPrivacyDataService
    {
        /*
         * Review comment 
         * no need to spend resources on wrapping in Task.
         * better to return ValueTask
         */
        public ValueTask<string> GetPrivacyDataAsync()
        {
            return new ValueTask<string>("This Policy describes how async/await processes your personal data," +
                                            "but it may not address all possible data processing scenarios.");
        }
    }
}
