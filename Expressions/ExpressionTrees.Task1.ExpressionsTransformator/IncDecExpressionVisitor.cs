﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ExpressionTrees.Task1.ExpressionsTransformer
{
    public class IncDecExpressionVisitor : ExpressionVisitor
    {
        protected override Expression VisitBinary(BinaryExpression node)
        {
            if ((node.NodeType == ExpressionType.Add
                  && ((node.Left.NodeType == ExpressionType.Parameter && node.Right.NodeType == ExpressionType.Constant)
                       || (node.Right.NodeType == ExpressionType.Parameter && node.Left.NodeType == ExpressionType.Constant)))
                 || (node.NodeType == ExpressionType.Subtract && node.Left.NodeType == ExpressionType.Parameter && node.Right.NodeType == ExpressionType.Constant))
            {

                (ParameterExpression param, ConstantExpression constant) tupl = node.Left.NodeType == ExpressionType.Parameter
                    ? ((ParameterExpression)node.Left, (ConstantExpression)node.Right)
                    : ((ParameterExpression)node.Right, (ConstantExpression)node.Left);



                if (tupl.constant.Type == typeof(int) && (int)tupl.constant.Value == 1)
                {
                    return node.NodeType == ExpressionType.Add ? Expression.Increment(tupl.param) : Expression.Decrement(tupl.param);
                }
                else if (tupl.constant.Type == typeof(int) && (int)tupl.constant.Value == -1)
                {
                    return Expression.Decrement(tupl.param);
                }
            }

            return base.VisitBinary(node);
        }
    }

    public class ReplacerVisitor : ExpressionVisitor
    {
        Dictionary<string, int> _params;

        public ReplacerVisitor(Dictionary<string, int> paramsDictionary)
        {
            _params = paramsDictionary;
        }

        protected override Expression VisitBinary(BinaryExpression node)
        {
            if (node.Left.NodeType == ExpressionType.Parameter || node.Right.NodeType == ExpressionType.Parameter)
            {
                ConstantExpression constantLeft = null;
                ConstantExpression constantRight = null;

                if (node.Left.NodeType == ExpressionType.Parameter)
                {
                    var paramLeft = (ParameterExpression)node.Left;

                    if (_params.ContainsKey(paramLeft.Name))
                    {
                        constantLeft = Expression.Constant(_params[paramLeft.Name]);
                    }

                }

                if (node.Right.NodeType == ExpressionType.Parameter)
                {
                    var paramRight = (ParameterExpression)node.Right;

                    if (_params.ContainsKey(paramRight.Name))
                    {
                        constantRight = Expression.Constant(_params[paramRight.Name]);
                    }
                }

                switch (node.NodeType)
                {
                    case ExpressionType.Add:
                        return Expression.Add(constantLeft ?? node.Left, constantRight ?? node.Right);
                    case ExpressionType.Subtract:
                        return Expression.Subtract(constantLeft ?? node.Left, constantRight ?? node.Right);
                    case ExpressionType.Multiply:
                        return Expression.Multiply(constantLeft ?? node.Left, constantRight ?? node.Right);
                    case ExpressionType.Divide:
                        return Expression.Multiply(constantLeft ?? node.Left, constantRight ?? node.Right);
                   /* case ExpressionType.AddAssign:
                    case ExpressionType.AddAssignChecked:
                    case ExpressionType.AddChecked:
                    case ExpressionType.And:
                    case ExpressionType.AndAlso:
                    case ExpressionType.AndAssign:
                    case ExpressionType.ArrayIndex:
                    case ExpressionType.ArrayLength:
                    case ExpressionType.Assign:
                    case ExpressionType.Block:
                    case ExpressionType.Call:
                    case ExpressionType.Coalesce:
                    case ExpressionType.Conditional:
                    case ExpressionType.Constant:
                    case ExpressionType.Convert:
                    case ExpressionType.ConvertChecked:
                    case ExpressionType.DebugInfo:
                    case ExpressionType.Decrement:
                    case ExpressionType.Default:
                    case ExpressionType.DivideAssign:
                    case ExpressionType.Dynamic:
                    case ExpressionType.Equal:
                    case ExpressionType.ExclusiveOr:
                    case ExpressionType.ExclusiveOrAssign:
                    case ExpressionType.Extension:
                    case ExpressionType.Goto:
                    case ExpressionType.GreaterThan:
                    case ExpressionType.GreaterThanOrEqual:
                    case ExpressionType.Increment:
                    case ExpressionType.Index:
                    case ExpressionType.Invoke:
                    case ExpressionType.IsFalse:
                    case ExpressionType.IsTrue:
                    case ExpressionType.Label:
                    case ExpressionType.Lambda:
                    case ExpressionType.LeftShift:
                    case ExpressionType.LeftShiftAssign:
                    case ExpressionType.LessThan:
                    case ExpressionType.LessThanOrEqual:
                    case ExpressionType.ListInit:
                    case ExpressionType.Loop:
                    case ExpressionType.MemberAccess:
                    case ExpressionType.MemberInit:
                    case ExpressionType.Modulo:
                    case ExpressionType.ModuloAssign:
                    case ExpressionType.MultiplyAssign:
                    case ExpressionType.MultiplyAssignChecked:
                    case ExpressionType.MultiplyChecked:
                    case ExpressionType.Negate:
                    case ExpressionType.NegateChecked:
                    case ExpressionType.New:
                    case ExpressionType.NewArrayBounds:
                    case ExpressionType.NewArrayInit:
                    case ExpressionType.Not:
                    case ExpressionType.NotEqual:
                    case ExpressionType.OnesComplement:
                    case ExpressionType.Or:
                    case ExpressionType.OrAssign:
                    case ExpressionType.OrElse:
                    case ExpressionType.Parameter:
                    case ExpressionType.PostDecrementAssign:
                    case ExpressionType.PostIncrementAssign:
                    case ExpressionType.Power:
                    case ExpressionType.PowerAssign:
                    case ExpressionType.PreDecrementAssign:
                    case ExpressionType.PreIncrementAssign:
                    case ExpressionType.Quote:
                    case ExpressionType.RightShift:
                    case ExpressionType.RightShiftAssign:
                    case ExpressionType.RuntimeVariables:
                    case ExpressionType.SubtractAssign:
                    case ExpressionType.SubtractAssignChecked:
                    case ExpressionType.SubtractChecked:
                    case ExpressionType.Switch:
                    case ExpressionType.Throw:
                    case ExpressionType.Try:
                    case ExpressionType.TypeAs:
                    case ExpressionType.TypeEqual:
                    case ExpressionType.TypeIs:
                    case ExpressionType.UnaryPlus:
                    case ExpressionType.Unbox:
                    default:
                        return base.VisitBinary(node);*/
                }
            }

            return base.VisitBinary(node);
        }
    }
}
