﻿/*
 * Create a class based on ExpressionVisitor, which makes expression tree transformation:
 * 1. converts expressions like <variable> + 1 to increment operations, <variable> - 1 - into decrement operations.
 * 2. changes parameter values in a lambda expression to constants, taking the following as transformation parameters:
 *    - source expression;
 *    - dictionary: <parameter name: value for replacement>
 * The results could be printed in console or checked via Debugger using any Visualizer.
 */
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace ExpressionTrees.Task1.ExpressionsTransformer
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Expression Visitor for increment/decrement.");
            Console.WriteLine();

            Expression<Func<int, int, int>> expr = (x, y) => (x - 1) * (x + 2) * (-1 + x + 1 + (1 + y)) + (x * 4) + (y / 1) - (x - 1);

            var result = new IncDecExpressionVisitor().VisitAndConvert(expr, "");


            Console.WriteLine(expr);
            Console.WriteLine(result);
            Console.WriteLine();
            Console.WriteLine(expr.Compile().Invoke(2,3));
            Console.WriteLine(result.Compile().Invoke(2,3));
            Console.WriteLine();

            var paramsDictionary = new Dictionary<string, int>
            {
                ["x"] = 7,
                ["y"] = 3
            };

            var replaceResult = new ReplacerVisitor(paramsDictionary).VisitAndConvert(expr, "");

            Console.WriteLine(expr);
            Console.WriteLine(replaceResult);
            Console.WriteLine();
            Console.WriteLine(expr.Compile().Invoke(7, 3));
            Console.WriteLine(replaceResult.Compile().Invoke(0, 0));

            Console.ReadLine();
        }
    }
}
