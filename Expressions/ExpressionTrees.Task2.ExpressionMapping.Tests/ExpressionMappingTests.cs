using ExpressionTrees.Task2.ExpressionMapping.Tests.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace ExpressionTrees.Task2.ExpressionMapping.Tests
{
    [TestClass]
    public class ExpressionMappingTests
    {
        // todo: add as many test methods as you wish, but they should be enough to cover basic scenarios of the mapping generator

        [TestMethod]
        public void TestMethod1()
        {
            var foo = new Foo
            {
                Property1 = 1,
                Property2 = "1",
                Property3 = true,
                Property4 = 2,
                Property5 = "3",
                Property6 = null,
            };

            var customMapping = new Dictionary<string, string>()
            {
                [$"{nameof(Foo.Property6)}"] = $"{nameof(Bar.Property8)}",
            };

            var mapGenerator = new MappingGenerator()
                .AddCustomMapping(customMapping)
                .AddCustomMapping<Foo, Bar>(f => f.Property1, b => b.Property1)
                .AddCustomMapping<Foo, Bar>(f => f.Property5, b => b.Property7);

            var mapper = mapGenerator.Generate<Foo, Bar>();

            var res = mapper.Map(foo);

            Assert.AreEqual(res.Property1, foo.Property1);
            Assert.AreEqual(res.Property7, foo.Property5);
            Assert.AreEqual(res.Property8, foo.Property6);
        }
    }
}
