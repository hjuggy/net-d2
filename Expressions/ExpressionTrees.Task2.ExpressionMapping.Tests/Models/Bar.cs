﻿namespace ExpressionTrees.Task2.ExpressionMapping.Tests.Models
{
    internal class Bar
    {
        public int Property1 { get; set; }

        public string Property2 { get; set; }

        public bool Property3 { get; set; }

        public int? Property4 { get; set; }

        public string Property7 { get; set; }

        public bool? Property8 { get; set; }
    }
}
