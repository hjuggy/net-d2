﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace ExpressionTrees.Task2.ExpressionMapping
{
    public class MappingGenerator
    {
        private Dictionary<string, string> _customMapping = new Dictionary<string, string>();

        public Mapper<TSource, TDestination> Generate<TSource, TDestination>()
        {
            var newDestinationExpression = Expression.New(typeof(TDestination));
            var sourceParameter = Expression.Parameter(typeof(TSource));
            var memberInitExpr = Expression.MemberInit(newDestinationExpression, CreateMemberBindings(sourceParameter, typeof(TDestination)));

            var mapFunction = Expression.Lambda<Func<TSource, TDestination>>(memberInitExpr, sourceParameter);
            return new Mapper<TSource, TDestination>(mapFunction.Compile());
        }

        public MappingGenerator AddCustomMapping(Dictionary<string, string> customMapping)
        {
            _customMapping = customMapping;
            return this;
        }

        public MappingGenerator AddCustomMapping<TSource, TDestination>(Expression<Func<TSource, object>> s, Expression<Func<TDestination, object>> d)
        {
            string sourceName;
            string destName;
            if (s.Body.NodeType == ExpressionType.Convert)
            {
                sourceName = ((s.Body as UnaryExpression).Operand as MemberExpression).Member.Name;
            }
            else if (s.Body.NodeType == ExpressionType.MemberAccess)
            {
                sourceName = (s.Body as MemberExpression).Member.Name;
            }
            else
            {
                // if class then to do recursion for properties DTO
                 throw new NotImplementedException("Not Implemented for class");
            }

            if (d.Body.NodeType == ExpressionType.Convert)
            {
                destName = ((d.Body as UnaryExpression).Operand as MemberExpression).Member.Name;
            }
            else if (d.Body.NodeType == ExpressionType.MemberAccess)
            {
                destName = (d.Body as MemberExpression).Member.Name;
            }
            else
            {
                // if class then to do recursion for properties DTO
                throw new NotImplementedException("Not Implemented for class");
            }
            _customMapping.Add(sourceName, destName);
            return this;
        }

        private List<MemberBinding> CreateMemberBindings(ParameterExpression sourceParameter, Type destinationType)
        {
            var result = new List<MemberBinding>();

            var sourceProperties = sourceParameter.Type.GetProperties();
            var destinationProperties = destinationType.GetProperties();

            foreach (var sourceProperty in sourceProperties)
            {
                var propertyInfo = _customMapping.TryGetValue(sourceProperty.Name, out var destinationPropertyName) ?
                    destinationProperties.FirstOrDefault(p => p.Name == destinationPropertyName) :
                    destinationProperties.FirstOrDefault(p => p.Name == sourceProperty.Name);

                if (propertyInfo is null)
                    continue;

                var access = Expression.MakeMemberAccess(sourceParameter, sourceProperty);
                var assign = Expression.Bind(propertyInfo, access);
                result.Add(assign);
            }

            return result;
        }
    }
}
