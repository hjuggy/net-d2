﻿namespace StockExchange.Task1
{
    public class Blossomers : IStockPlayer
    {
        private readonly IStockMediator _stokMediator;

        public Blossomers(IStockMediator stokMediator)
        {
            _stokMediator = stokMediator;
        }

        public bool SellOffer(string stockName, int numberOfShares)
        {
            return _stokMediator.TryMakeDeal(this, OfferType.Sell, stockName, numberOfShares);
        }

        public bool BuyOffer(string stockName, int numberOfShares)
        {
            return _stokMediator.TryMakeDeal(this, OfferType.Buy, stockName, numberOfShares);
        }
    }
}
