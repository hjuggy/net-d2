﻿namespace StockExchange.Task1
{
    public interface IStockMediator
    {
        bool TryMakeDeal(IStockPlayer stockPlayer, OfferType offerType, string stockName, int numberOfShares);
    }
}