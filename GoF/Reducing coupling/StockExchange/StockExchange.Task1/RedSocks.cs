﻿namespace StockExchange.Task1
{
    public class RedSocks : IStockPlayer
    {
        private readonly IStockMediator _stockMediator;

        public RedSocks(IStockMediator stockMediator)
        {
            _stockMediator = stockMediator;
        }

        public bool SellOffer(string stockName, int numberOfShares)
        {
            return _stockMediator.TryMakeDeal(this, OfferType.Sell, stockName, numberOfShares);
        }

        public bool BuyOffer(string stockName, int numberOfShares)
        {
            return _stockMediator.TryMakeDeal(this, OfferType.Buy, stockName, numberOfShares);
        }
    }
}
