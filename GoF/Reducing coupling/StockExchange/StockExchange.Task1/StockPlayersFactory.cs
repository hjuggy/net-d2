﻿namespace StockExchange.Task1
{
    public class StockPlayersFactory
    {
        public Players CreatePlayers()
        {
            var mediator = new StockMediator();

            return new Players
            {
                RedSocks = new RedSocks(mediator),
                Blossomers = new Blossomers(mediator)
            };
        }
    }
}
