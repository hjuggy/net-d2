﻿using StockExchange.Task2.Player;

namespace StockExchange.Task2.Mediator
{
    public interface IStockMediator
    {
        bool TryMakeDeal(Offer offer);
    }
}