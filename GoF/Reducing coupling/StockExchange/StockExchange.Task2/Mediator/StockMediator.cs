﻿using StockExchange.Task2.Player;
using System.Collections.Generic;
using System.Linq;

namespace StockExchange.Task2.Mediator
{
    public class StockMediator : IStockMediator
    {
        private readonly List<Offer> _offers = new List<Offer>();

        public bool TryMakeDeal(Offer offer)
        {
            var suitableOffer = _offers.FirstOrDefault(o => o.PlayerId != offer.PlayerId && o.StockName == offer.StockName
                && o.OfferType != offer.OfferType && o.NumberOfShares == offer.NumberOfShares);

            if (suitableOffer != null)
            {
                _offers.Remove(suitableOffer);
                return true;
            }

            _offers.Add(offer);
            return false;
        }
    }
}