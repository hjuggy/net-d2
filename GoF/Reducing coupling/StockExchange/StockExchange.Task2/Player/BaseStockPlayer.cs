﻿using StockExchange.Task2.Mediator;
using System;

namespace StockExchange.Task2.Player
{
    public abstract class BaseStockPlayer : IStockPlayer
    {
        private readonly IStockMediator _stockMediator;

        protected BaseStockPlayer(IStockMediator stockMediator, Guid id)
        {
            _stockMediator = stockMediator;
            Id = id;
        }

        public Guid Id { get; }

        public bool SellOffer(string stockName, int numberOfShares)
        {
            return _stockMediator.TryMakeDeal(new Offer(Id, stockName, OfferType.Sell, numberOfShares));
        }

        public bool BuyOffer(string stockName, int numberOfShares)
        {
            return _stockMediator.TryMakeDeal(new Offer(Id, stockName, OfferType.Buy, numberOfShares));
        }
    }
}