﻿using System;

namespace StockExchange.Task2.Player
{
    public class Offer
    {
        public Offer(Guid playerId, string stockName, OfferType offerType, int numberOfShares)
        {
            PlayerId = playerId;
            StockName = stockName;
            OfferType = offerType;
            NumberOfShares = numberOfShares;
        }

        public OfferType OfferType { get; }

        public string StockName { get; }

        public int NumberOfShares { get; }

        public Guid PlayerId { get; }
    }
}