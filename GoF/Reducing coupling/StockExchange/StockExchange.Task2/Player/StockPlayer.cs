﻿using StockExchange.Task2.Mediator;
using System;

namespace StockExchange.Task2.Player
{
    public class StockPlayer : BaseStockPlayer
    {
        public StockPlayer(IStockMediator stockMediator, Guid id) : base(stockMediator, id)
        {
        }
    }
}