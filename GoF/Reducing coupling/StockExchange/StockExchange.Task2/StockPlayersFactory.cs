﻿using StockExchange.Task2.Mediator;
using StockExchange.Task2.Player;
using System;

namespace StockExchange.Task2
{
    public class StockPlayersFactory
    {
        IStockMediator mediator;

        public StockPlayersFactory()
        {
            mediator = new StockMediator();
        }

        public StockPlayer CreatePlayer() => new StockPlayer(mediator, Guid.NewGuid());
    }
}
