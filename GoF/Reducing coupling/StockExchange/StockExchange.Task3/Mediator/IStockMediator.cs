﻿using StockExchange.Task3.Player;

namespace StockExchange.Task3.Mediator
{
    public interface IStockMediator
    {
        bool TryMakeDeal(IStockPlayer stockPlayer, OfferType offerType, string stockName, int numberOfShares);
    }
}