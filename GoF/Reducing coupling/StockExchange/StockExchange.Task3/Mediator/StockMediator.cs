﻿using StockExchange.Task3.Player;
using System.Collections.Generic;
using System.Linq;

namespace StockExchange.Task3.Mediator
{
    public class StockMediator : IStockMediator
    {
        private readonly List<Offer> _offers = new List<Offer>();

        public bool TryMakeDeal(IStockPlayer stockPlayer, OfferType offerType, string stockName, int numberOfShares)
        {
            var suitableOffer = _offers.FirstOrDefault(o => o.Player.Name != stockPlayer.Name && o.Type != offerType
                                                            && o.StockName == stockName && o.NumberOfShares == numberOfShares);

            if (suitableOffer != null)
            {
                _offers.Remove(suitableOffer);

                var isBuyDeal = suitableOffer.Type == OfferType.Buy;

                suitableOffer.Player.UpdateStatus(isBuyDeal ? (0, numberOfShares) : (numberOfShares, 0));
                stockPlayer.UpdateStatus(isBuyDeal ? (numberOfShares, 0) : (0, numberOfShares));

                return true;
            }

            _offers.Add(new Offer(offerType, stockName, numberOfShares, stockPlayer));

            return false;
        }

        private class Offer
        {
            public OfferType Type { get; }
            public string StockName { get; }
            public int NumberOfShares { get; }
            public IStockPlayer Player { get; }

            public Offer(OfferType type, string stockName, int numberOfShares, IStockPlayer player)
            {
                Type = type;
                StockName = stockName;
                NumberOfShares = numberOfShares;
                Player = player;
            }
        }
    }
}