﻿using StockExchange.Task3.Mediator;

namespace StockExchange.Task3.Player
{
    public abstract class BaseStockPlayer : IStockPlayer
    {
        private readonly IStockMediator _stokMediator;

        protected BaseStockPlayer(IStockMediator stokMediator, string name)
        {
            _stokMediator = stokMediator;
            Name = name;
        }

        public string Name { get;  }

        public int SoldShares { get; private set; }

        public int BoughtShares { get; private set; }

        public bool SellOffer(string stockName, int numberOfShares)
        {
            return _stokMediator.TryMakeDeal(this, OfferType.Sell, stockName, numberOfShares);
        }

        public bool BuyOffer(string stockName, int numberOfShares)
        {
            return _stokMediator.TryMakeDeal(this, OfferType.Buy, stockName, numberOfShares);
        }

        public void UpdateStatus((int sold, int bought) shares)
        {
            SoldShares += shares.sold;
            BoughtShares += shares.bought;
        }
    }
}