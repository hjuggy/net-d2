﻿namespace StockExchange.Task3.Player
{
    public interface IStockPlayer
    {
        int SoldShares { get; }

        int BoughtShares { get; }

        public string Name { get; }

        bool SellOffer(string stockName, int numberOfShares);

        bool BuyOffer(string stockName, int numberOfShares);

        void UpdateStatus((int sold, int bought) shares);
    }
}