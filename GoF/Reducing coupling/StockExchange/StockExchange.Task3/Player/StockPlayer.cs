﻿using StockExchange.Task3.Mediator;

namespace StockExchange.Task3.Player
{
    public class StockPlayer : BaseStockPlayer
    {
        public StockPlayer(IStockMediator mediator, string name) : base(mediator, name)
        {
        }
    }
}
