﻿using StockExchange.Task3.Mediator;
using StockExchange.Task3.Player;

namespace StockExchange.Task3
{
    public class StockPlayersFactory
    {
        IStockMediator mediator;

        public StockPlayersFactory()
        {
            mediator = new StockMediator();
        }

        public StockPlayer CreatePlayer(string name) => new StockPlayer(mediator, name);
    }
}
