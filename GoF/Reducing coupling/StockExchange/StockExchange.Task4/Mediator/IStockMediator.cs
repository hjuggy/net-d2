﻿using StockExchange.Task4.Player;
using System;

namespace StockExchange.Task4.Mediator
{
    public interface IStockMediator
    {
        event EventHandler<(int bought, int sold)> UpdatePlayerStatus;

        bool TryMakeDeal(IStockPlayer stockPlayer, OfferType offerType, string stockName, int numberOfShares);
    }
}