﻿using StockExchange.Task4.Mediator;

namespace StockExchange.Task4.Player
{
    public abstract class BaseStockPlayer : IStockPlayer
    {
        private readonly IStockMediator _stokMediator;

        protected BaseStockPlayer(IStockMediator stokMediator, string name)
        {
            Name = name;
            _stokMediator = stokMediator;
            _stokMediator.UpdatePlayerStatus += StokMediatorUpdatePlayerStatus;
        }

        private void StokMediatorUpdatePlayerStatus(object sender, (int bought, int sold) e)
        {
            if (sender != this) return;

            BoughtShares += e.bought;
            SoldShares += e.sold;
        }

        public string Name { get; }

        public int SoldShares { get; private set; }

        public int BoughtShares { get; private set; }

        public bool SellOffer(string stockName, int numberOfShares)
        {
            return _stokMediator.TryMakeDeal(this, OfferType.Sell, stockName, numberOfShares);
        }

        public bool BuyOffer(string stockName, int numberOfShares)
        {
            return _stokMediator.TryMakeDeal(this, OfferType.Buy, stockName, numberOfShares);
        }
    }
}