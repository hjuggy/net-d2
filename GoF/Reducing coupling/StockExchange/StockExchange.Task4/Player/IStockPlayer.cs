﻿namespace StockExchange.Task4.Player
{
    public interface IStockPlayer
    {
        string Name { get; }

        int SoldShares { get; }

        bool SellOffer(string stockName, int numberOfShares);

        bool BuyOffer(string stockName, int numberOfShares);
    }
}