﻿using StockExchange.Task4.Mediator;

namespace StockExchange.Task4.Player
{

    public class StockPlayer : BaseStockPlayer
    {
        public StockPlayer(IStockMediator stockMediator, string name) : base(stockMediator, name)
        {
        }
    }
}
