﻿using StockExchange.Task4.Mediator;
using StockExchange.Task4.Player;

namespace StockExchange.Task4
{
    public class StockPlayersFactory
    {
        IStockMediator mediator;

        public StockPlayersFactory()
        {
            mediator = new StockMediator();
        }

        public StockPlayer CreatePlayer(string name)
        {
            return new StockPlayer(mediator, name);
        }
    }
}
