﻿namespace Adapter.Task1
{
    public class MyPrinterFactory 
    {
        public IMyPrinter CreateMyPrinter(IWriter writer)
        {
            return new MyPrinterAdapter(new Printer(writer));
        }
    }

	public class MyPrinterAdapter : IMyPrinter
	{
		private readonly Printer printer;

		public MyPrinterAdapter(Printer printer)
		{
			this.printer = printer;
		}

		public void Print<T>(IElements<T> elements)
		{
			this.printer.Print(new Container<T>(elements.GetElements()));
		}
	}
}
