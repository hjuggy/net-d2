﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Composite.Task2.Tests
{
    [TestClass]
    public class FormTests
    {
        public FormTests()
        {
        }

        [TestMethod]
        public void Should_convert_form_with_two_components_to_string()
        {
            var label = new LabelText("myLabel");
            var input = new InputText("myInput", "myInputValue");

            var form = new Form("myForm");
            form.AddComponent(label);
            form.AddComponent(input);

            var formStr = form.ConvertToString();
            var expected = "<form name='myForm'>\r\n\t<label value='myLabel'/>\r\n\t<inputText name='myInput' value='myInputValue'/>\r\n</form>";

            formStr.Should().Be(expected);
        }

        [TestMethod]
        public void Should_convert_form_with_internal_form_with_two_components_to_string()
        {
            var labelInternal = new LabelText("myLabelInternal");
            var inputInternal = new InputText("myInputInternal", "myInputInternalValue");

            var formInternal = new Form("myFormInternal");
            formInternal.AddComponent(labelInternal);
            formInternal.AddComponent(inputInternal);

            var label = new LabelText("myLabel");
            var input = new InputText("myInput", "myInputValue");

            var form = new Form("myForm");
            form.AddComponent(label);
            form.AddComponent(input);
            form.AddComponent(formInternal);

            var formStr = form.ConvertToString();
            var expected = "<form name='myForm'>\r\n\t<label value='myLabel'/>\r\n\t<inputText name='myInput' value='myInputValue'/>\r\n\t<form name='myFormInternal'>\r\n\t\t<label value='myLabelInternal'/>\r\n\t\t<inputText name='myInputInternal' value='myInputInternalValue'/>\r\n\t</form>\r\n</form>";

            formStr.Should().Be(expected);
        }
    }
}
