﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Composite.Task2
{
    public class Form : IComponent
    {
        string name;
        private List<IComponent> components =  new List<IComponent>();

        public Form(string name)
        {
            this.name = name;
        }

        public void AddComponent(IComponent component)
        {
            components.Add(component);
        }

        public string ConvertToString(int depth = 0)
        {
            string tabs = new string('\t', depth);

            StringBuilder builder = new StringBuilder();

            int childrenDepth = ++depth;
            builder.AppendLine($"{tabs}<form name='{name}'>");
            foreach (var component in components)
            {
                builder.AppendLine(component.ConvertToString(childrenDepth));
            }

            builder.Append($"{tabs}</form>");

            return builder.ToString();
        }
    }
}