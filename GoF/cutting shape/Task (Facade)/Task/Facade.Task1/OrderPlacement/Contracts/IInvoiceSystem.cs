﻿namespace Facade.Task1.OrderPlacement
{
    public interface IInvoiceSystem
    {
        void SendInvoice(Invoice invoice);
    }
}
