﻿namespace Facade.Task1.OrderPlacement
{
    public interface IPaymentSystem
    {
        bool MakePayment(Payment payment);
    }
}
