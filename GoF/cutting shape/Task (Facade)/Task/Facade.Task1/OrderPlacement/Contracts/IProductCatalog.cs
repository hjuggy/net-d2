﻿namespace Facade.Task1.OrderPlacement
{
    public interface IProductCatalog
    {
        Product GetProductDetails(string productId);
    }
}
