﻿using System;

namespace MyLinqProvider.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    class EntityColAttribute : Attribute
    {
        public EntityColAttribute()
        {
        }

        public EntityColAttribute(string name)
        {
            this.Name = name;
        }

        public string Name { get; set; }
    }
}
