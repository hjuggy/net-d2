﻿using System;

namespace MyLinqProvider.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    class EntityTableAttribute : Attribute
    {
        public EntityTableAttribute()
        {
        }

        public EntityTableAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
