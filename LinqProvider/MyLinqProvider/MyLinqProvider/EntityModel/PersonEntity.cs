﻿using MyLinqProvider.Attributes;

namespace MyLinqProvider.EntityModel
{
    [EntityTableAttribute(Name = "person")]
    class PersonEntity
    {
        [EntityCol(Name ="name")]
        public string FirstName { get; set; }
        [EntityCol(Name = "sur_name")]
        public string SurName { get; set; }
        [EntityCol(Name = "age")]
        public int Age { get; set; }
    }
}
