﻿using MyLinqProvider.EntityModel;
using System;

namespace MyLinqProvider
{
    internal class Program
    {
        static void Main()
        {
            var tableSet = new TableSet<PersonEntity>();

            var query = tableSet.Where(p => p.FirstName == "Tom" && p.Age > 21);

            Console.WriteLine(query);

            Console.ReadLine();
        }
    }

}
