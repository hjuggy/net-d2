﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MyLinqProvider
{
    public class SelectSqlGenerator<TResultModel, TModel> : IEnumerable<TResultModel>
		where TModel : class
	{
		private readonly Expression<Func<TModel, bool>> filterExpression;
		private readonly Expression<Func<TModel, TResultModel>> selectorExpression;
		private readonly TableSet<TModel> tableSet;

		public SelectSqlGenerator(
			TableSet<TModel> table,
			Expression<Func<TModel, bool>> filterExpression,
			Expression<Func<TModel, TResultModel>> selectorExpression)
		{
			tableSet = table;
			this.filterExpression = filterExpression;
			this.selectorExpression = selectorExpression;
		}

		private string GetSelectClauseString()
		{
			var newExpression = (NewExpression)selectorExpression.Body;

			var selectParam = newExpression.Arguments.Select(
				argumentExpression =>
				{
					return tableSet.Prop.First(p => p.PropName == ((MemberExpression)argumentExpression).Member.Name).colName;
				});

			return string.Join(",", selectParam);
		}

		public override string ToString()
		{
			return string.Format(
				"SELECT {0} FROM {1} WHERE {2}",
				GetSelectClauseString(),
				tableSet.TableName,
				filterExpression.Body);
		}

		public IEnumerator<TResultModel> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return (IEnumerator)GetEnumerator();
		}
	}

}
