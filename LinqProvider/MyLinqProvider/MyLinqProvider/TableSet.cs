﻿using MyLinqProvider.Attributes;
using MyLinqProvider.Helper;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace MyLinqProvider
{
    public class TableSet<TModel> where TModel : class
	{
		public string TableName { private set; get; }

		public MyQueryTranslator queryTranslator { private set; get; }

		public (string PropName, string colName, Type type)[] Prop { get; private set; }

		public TableSet()
		{
			InitTable();
			queryTranslator = new MyQueryTranslator();
		}

        public WhereSqlGenerator<TModel> Where(Expression<Func<TModel, bool>> filterExpression)
        {
            return new WhereSqlGenerator<TModel>(this, filterExpression);
        }

        public override string ToString()
        {
            return string.Format("SELECT * FROM {0}", TableName);
        }

        private void InitTable()
		{
			var type = typeof(TModel);

			var entityTableAttribute = type.GetCustomAttribute<EntityTableAttribute>();

            TableName = entityTableAttribute != null ? entityTableAttribute.Name : type.Name;

            var properties = type.GetProperties();
			Prop = new (string propName, string colName, Type type)[properties.Length];

            for (int i = 0; i < properties.Length; i++)
            {
				var entityColAttribute = properties[i].GetCustomAttribute<EntityColAttribute>();

				Prop[i] = (
					properties[i].Name,
					entityColAttribute != null ? entityColAttribute.Name : properties[i].Name,
					properties[i].PropertyType
					);
			}
		}
	}

}
