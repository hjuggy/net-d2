﻿using System;
using System.Linq.Expressions;

namespace MyLinqProvider
{
    public class WhereSqlGenerator<TModel> where TModel : class
	{
		private readonly Expression<Func<TModel, bool>> filterExpression;
		private readonly TableSet<TModel> tableSet;

		public WhereSqlGenerator(
			TableSet<TModel> table,
			Expression<Func<TModel, bool>> expression)
		{
			tableSet = table;
			this.filterExpression = expression;
		}

		public SelectSqlGenerator<TResultModel, TModel> Select<TResultModel>(
			Expression<Func<TModel, TResultModel>> selectorExpression)
		{
			return new SelectSqlGenerator<TResultModel, TModel>(
				tableSet,
				filterExpression,
				selectorExpression);
		}

		public override string ToString()
		{
			return string.Format(
				"SELECT * FROM {0} WHERE {1}",
				tableSet.TableName,
				tableSet.queryTranslator.Translate(filterExpression.Body));
		}
	}
}
