﻿namespace Common
{
    public class MessageModel
    {
        public string FileName { get; set; }

        public string MessageName { get; set; }

        public int Position { get; set; }

        public byte[] Filebody { get; set; }
    }

    public class FileInfoModel
    {
        public string FileName { get; set; }

        public string FileNameWithoutExtension { get; set; }

        public int Length { get; set; }

        public int Count { get; set; }
    }
}
