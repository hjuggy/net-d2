﻿using Common;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Consumer
{
    class Program
    {
        private static string dirReceived = @"C:\Messaging\received";

        static void Main(string[] args)
        {
            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672"),
            };

            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(
                "file-send-queue",
                true,
                false,
                false,
                null);

            var consumer = new EventingBasicConsumer(channel);


            consumer.Received += (object sender, BasicDeliverEventArgs e) =>
            {
                if (!Directory.Exists(dirReceived))
                {
                    Directory.CreateDirectory(dirReceived);
                }

                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                var fileInfo = JsonConvert.DeserializeObject<FileInfoModel>(message);

                byte[] filebody = GetBody(fileInfo, factory);

                File.WriteAllBytes(dirReceived + "\\" + fileInfo.FileName, filebody);
            };

            channel.BasicConsume("file-send-queue", true, consumer);

            Console.Read();

            //while (true)
            //{
            //    Task.Factory.StartNew(ReadBodyFile);
            //}
        }

        private static byte[] GetBody(FileInfoModel fileInfo, ConnectionFactory factory)
        {
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(
                fileInfo.FileNameWithoutExtension,
                true,
                false,
                false,
                null);

            var consumer = new EventingBasicConsumer(channel);

            var list = new List<MessageModel>();

            consumer.Received += (object sender, BasicDeliverEventArgs e) =>
            {
                var body = e.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                var mes = JsonConvert.DeserializeObject<MessageModel>(message);

                list.Add(mes);
            };

            channel.BasicConsume(fileInfo.FileNameWithoutExtension, true, consumer);

            while (list.Count != fileInfo.Count) { }

            return list.OrderBy(p => p.Position).ToList().SelectMany(p => p.Filebody).ToArray();
        }
    }
}
