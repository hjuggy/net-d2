﻿using Common;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Producer
{
    class Program
    {
        private static string dirToSend = @"C:\Messaging\toSend";

        private static string fileExtension = "*.txt";

        private static int size = 10000000;

        ///docker run -d --hostname messaging-rabbit --name my-rabbit -p 15672:15672 -p 5672:5672 rabbitmq:3-management

        static void Main()
        {
            Settings();

            WatchNewFile();
        }

        private static void Settings()
        {
            var inValid = true;
            do
            {
                Console.WriteLine("Input the path or press enter to use the default (C:\\Messaging\\toSend):");
                var path = Console.ReadLine().Trim();

                if (string.IsNullOrEmpty(path)) 
                {
                    inValid = false;
                }
                else
                {
                    if (Directory.Exists(path))
                    {
                        dirToSend = path;
                        inValid = false;
                    }
                    else
                    {
                        Console.WriteLine("Path is invalid, try again:");
                    }
                }
            } while (inValid);

            inValid = true;
            do
            {
                Console.WriteLine("Input file extension (example format *.pdf) or press enter to use the default (*.txt):");
                var fe = Console.ReadLine().Trim();

                if (string.IsNullOrEmpty(fe))
                {
                    inValid = false;
                }
                else
                {
                    if (fe.Contains("*."))
                    {
                        fileExtension = fe;
                        inValid = false;
                    }
                    else
                    {
                        Console.WriteLine("File extension is invalid, try again:");
                    }
                }
            } while (inValid);

        }

        static void WatchNewFile()
        {
            if (!Directory.Exists(dirToSend))
            {
                Directory.CreateDirectory(dirToSend);
            }

            using var watcher = new FileSystemWatcher(dirToSend);

            watcher.NotifyFilter = NotifyFilters.Attributes
                                 | NotifyFilters.CreationTime
                                 | NotifyFilters.DirectoryName
                                 | NotifyFilters.FileName
                                 | NotifyFilters.LastAccess
                                 | NotifyFilters.LastWrite
                                 | NotifyFilters.Security
                                 | NotifyFilters.Size;

            watcher.Created += SendFileOnCreate;
            watcher.Error += OnError;

            watcher.Filter = fileExtension;
            watcher.IncludeSubdirectories = true;
            watcher.EnableRaisingEvents = true;

            Console.Read();
        }

        static void SendFileOnCreate(object sender, FileSystemEventArgs e)
        {
            var fileBody = GetBinaryFile(e.FullPath);

            var fileInfo = new FileInfoModel
            {
                FileName = e.Name,
                Length = fileBody.Length,
                Count = GetCount(fileBody),
                FileNameWithoutExtension = Path.GetFileNameWithoutExtension(e.FullPath),
            };

            var messages = SplittingFile(fileInfo, fileBody);

            var factory = new ConnectionFactory
            {
                Uri = new Uri("amqp://guest:guest@localhost:5672"),
            };

            SendfileInfo(fileInfo, factory);

            SendMessages(messages, fileInfo, factory);
        }

        private static void SendfileInfo(FileInfoModel fileInfo, ConnectionFactory factory)
        {
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            channel.QueueDeclare(
                "file-send-queue",
                true,
                false,
                false,
                null);

            var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(fileInfo));

            channel.BasicPublish("", "file-send-queue", null, body);
        }

        private static void SendMessages(List<MessageModel> messages, FileInfoModel fileInfo, ConnectionFactory factory)
        {
            using var connection = factory.CreateConnection();
            using var channel = connection.CreateModel();

            foreach (var message in messages)
            {
                channel.QueueDeclare(
                    message.MessageName,
                    true,
                    false,
                    false,
                    null);

                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(message));

                channel.BasicPublish("", message.MessageName, null, body);
            }
        }

        private static int GetCount(byte[] fileBody)
        {
            int count = fileBody.Length / size;

            if (count != fileBody.Length * size)
            {
                count++;
            }

            return count;
        }

        static byte[] GetBinaryFile(string filename)
        {
            bool fileInUse;

            do
            {
                fileInUse = FileInUse(filename);
            } while (fileInUse);

            byte[] bytes;
            using FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read);
            bytes = new byte[file.Length];
            file.Read(bytes, 0, (int)file.Length);

            return bytes;
        }

        static bool FileInUse(string path)
        {
            try
            {
                using FileStream fs = new FileStream(path, FileMode.OpenOrCreate);

                fs.Close();

                return false;
            }
            catch (IOException ex)
            {
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine("Stacktrace:");
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine("file in use");
                return true;
            }
        }

        static void OnError(object sender, ErrorEventArgs e) =>
            PrintException(e.GetException());

        static void PrintException(Exception ex)
        {
            if (ex != null)
            {
                Console.WriteLine($"Message: {ex.Message}");
                Console.WriteLine("Stacktrace:");
                Console.WriteLine(ex.StackTrace);
                Console.WriteLine();
                PrintException(ex.InnerException);
            }
        }

        static List<MessageModel> SplittingFile(FileInfoModel fileInfo, byte[] binaryFile)
        {
            var messages = new List<MessageModel>();

            var length = binaryFile.Length;

            var skip = 0;

            var position = 0;

            do
            {
                var take = length > size ? size : length;

                messages.Add(new MessageModel
                {
                     FileName = fileInfo.FileName,
                     Filebody = binaryFile.Skip(skip).Take(take).ToArray(),
                     Position = position,
                     MessageName = fileInfo.FileNameWithoutExtension,
                });

                position++;
                length -= size;

            } while (length >= 0);


            return messages;
        }


    }
}
