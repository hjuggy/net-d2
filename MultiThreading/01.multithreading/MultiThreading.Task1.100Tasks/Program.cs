﻿/*
 * 1.	Write a program, which creates an array of 100 Tasks, runs them and waits all of them are not finished.
 * Each Task should iterate from 1 to 1000 and print into the console the following string:
 * “Task #0 – {iteration number}”.
 */
using System;
using System.Threading.Tasks;

namespace MultiThreading.Task1._100Tasks
{
    class Program
    {
        const int TaskAmount = 100;
        const int MaxIterationsCount = 1000;

        static void Main(string[] args)
        {
            Console.WriteLine(".Net Mentoring Program. Multi threading V1.");
            Console.WriteLine("1.	Write a program, which creates an array of 100 Tasks, runs them and waits all of them are not finished.");
            Console.WriteLine("Each Task should iterate from 1 to 1000 and print into the console the following string:");
            Console.WriteLine("“Task #0 – {iteration number}”.");
            Console.WriteLine();
            
            HundredTasks();

            Console.ReadLine();
        }

        static void HundredTasks()
        {
            Task[] taskArray = new Task[TaskAmount];


            // 1st way used Parallel.For
            Parallel.For(0, TaskAmount, (i) =>
            {
                taskArray[i] = Task.Factory.StartNew(() =>
                {
                    Parallel.For(1, MaxIterationsCount, (j) => Output(i, j));
                });
            });

            //// 2nd way used for and  an additional object that contains the required context of the current iteration
            //for (int i = 0; i < TaskAmount; i++)
            //{
            //    taskArray[i] = Task.Factory.StartNew((Object o) =>
            //    {
            //        var obj = o as ObjectTask;
            //        Parallel.For(1, MaxIterationsCount, (j) => Output(obj.TaskNumber, j));
            //    },
            //    new ObjectTask { TaskNumber = i});
            //}

            Task.WaitAll(taskArray);
        }

        static void Output(int taskNumber, int iterationNumber)
        {
            Console.WriteLine($"Task #{taskNumber} – {iterationNumber}");
        }
    }

    //object that contains the required context of the current iteration
    public class ObjectTask
    {
        public int TaskNumber { get; set; }
    }
}
