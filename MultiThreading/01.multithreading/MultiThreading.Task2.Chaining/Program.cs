﻿/*
 * 2.	Write a program, which creates a chain of four Tasks.
 * First Task – creates an array of 10 random integer.
 * Second Task – multiplies this array with another random integer.
 * Third Task – sorts this array by ascending.
 * Fourth Task – calculates the average value. All this tasks should print the values to console.
 */
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MultiThreading.Task2.Chaining
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine(".Net Mentoring Program. MultiThreading V1 ");
            Console.WriteLine("2.	Write a program, which creates a chain of four Tasks.");
            Console.WriteLine("First Task – creates an array of 10 random integer.");
            Console.WriteLine("Second Task – multiplies this array with another random integer.");
            Console.WriteLine("Third Task – sorts this array by ascending.");
            Console.WriteLine("Fourth Task – calculates the average value. All this tasks should print the values to console");
            Console.WriteLine();

            // feel free to add your code

            var rnd = new Random();

            var rneee = rnd.Next(1, 100);
            var numbers = new int[10];

            await Task.Run(() =>
            {
                for (int i = 0; i < numbers.Length; i++)
                {
                    numbers[i] = rnd.Next(1, 100);
                }

                Output(numbers, "First Task");
            })
                .ContinueWith(antecedent =>
                {
                    for (int i = 0; i < numbers.Length; i++)
                    {
                        numbers[i] *= rnd.Next(1, 100);
                    }

                    Output(numbers, "Second Task");
                })
                .ContinueWith(antecedent =>
                {
                    Array.Sort(numbers);
                    Output(numbers, "Third Task");
                })
                .ContinueWith(antecedent =>
                {
                    var averageValue = numbers.Average();
                    Console.WriteLine($"Average value = {averageValue}");
                });

            Console.ReadLine();
        }

        static void Output(int[] numbers, string taskName)
        {
            Console.WriteLine($"##### Start {taskName} #####\n");

            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine($"pos {i} = \t {numbers[i]}");
            }

            Console.WriteLine($"##### End {taskName} #####\n");
        }
    }
}
