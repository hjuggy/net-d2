﻿using MultiThreading.Task3.MatrixMultiplier.Matrices;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task3.MatrixMultiplier.Multipliers
{
    public class MatricesMultiplierParallel : IMatricesMultiplier
    {
        public IMatrix Multiply(IMatrix m1, IMatrix m2)
        {
            var resultMatrix = new Matrix(m1.RowCount, m2.ColCount);
            
            Parallel.For(0, m2.ColCount, (i) =>
            {
                Parallel.For(0, m2.ColCount, (j) =>
                {
                    var sum = 0L;
                    Parallel.For(0, resultMatrix.RowCount,
                        (k, loopState) =>
                        {
                            var pow = m1.GetElement(i, k) * m2.GetElement(k, j);
                            sum += pow;
                        });

                    resultMatrix.SetElement(i, j, sum);
                });
        });

            return resultMatrix;
        }
    }
}
