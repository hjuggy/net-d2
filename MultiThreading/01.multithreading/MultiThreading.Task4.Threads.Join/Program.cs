﻿/*
 * 4.	Write a program which recursively creates 10 threads.
 * Each thread should be with the same body and receive a state with integer number, decrement it,
 * print and pass as a state into the newly created thread.
 * Use Thread class for this task and Join for waiting threads.
 * 
 * Implement all of the following options:
 * - a) Use Thread class for this task and Join for waiting threads.
 * - b) ThreadPool class for this task and Semaphore for waiting threads.
 */

using System;
using System.Threading;

namespace MultiThreading.Task4.Threads.Join
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("4.	Write a program which recursively creates 10 threads.");
            Console.WriteLine("Each thread should be with the same body and receive a state with integer number, decrement it, print and pass as a state into the newly created thread.");
            Console.WriteLine("Implement all of the following options:");
            Console.WriteLine();
            Console.WriteLine("- a) Use Thread class for this task and Join for waiting threads.");
            Console.WriteLine("- b) ThreadPool class for this task and Semaphore for waiting threads.");

            Console.WriteLine();

            TreadRun(new StateThread { Number = 10 });

            Console.WriteLine();
            Thread.Sleep(500);

            var sph = new Semaphore(1, 1);
            sph.WaitOne();
            ThreadPool.QueueUserWorkItem(TreadfromPoolRun, new StateThread { Number = 10, Sph = sph });

            Console.ReadLine();
        }

        public static void TreadRun(object obj)
        {
            var state = obj as StateThread;

            Console.WriteLine($"Thread with number of state: {state.Number} was started");
            if (state.Number > 0)
            {
                state.Number--;
                var th = new Thread(TreadRun);
                Console.WriteLine($"Thread with number of state: {state.Number} was created");
                th.Start(new StateThread { Number = state.Number });
                th.Join();
                Console.WriteLine($"Thread with number of state: {state.Number} was joined");
            }
        }

        public static void TreadfromPoolRun(object obj)
        {
            var state = obj as StateThread;

            if (state.Number == 0)
            {
                state.Sph.Release();
                return;
            }

            Thread.Sleep(10);
            Console.WriteLine($"Thread from ThreadPool with number of state: {state.Number} was started");

            if (state.Number > 0)
            {
                state.Number--;

                Console.WriteLine($"Thread from ThreadPool is colling. Number of state: {state.Number}");

                ThreadPool.QueueUserWorkItem(TreadfromPoolRun, new StateThread { Number = state.Number, Sph = state.Sph } );
                
                state.Sph.WaitOne();
                Console.WriteLine($"Thread from ThreadPool with number of state: {state.Number} was joined");
                state.Sph.Release();
            }
        
        }
    }

    public class StateThread
    {
        public int Number { get; set; }

        public Semaphore Sph { get; set; }
    }
}
