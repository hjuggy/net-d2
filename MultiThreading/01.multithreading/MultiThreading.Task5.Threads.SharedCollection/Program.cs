﻿/*
 * 5. Write a program which creates two threads and a shared collection:
 * the first one should add 10 elements into the collection and the second should print all elements
 * in the collection after each adding.
 * Use Thread, ThreadPool or Task classes for thread creation and any kind of synchronization constructions.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace MultiThreading.Task5.Threads.SharedCollection
{
    class Program
    {
        private static List<int> sharedCollection = new List<int>();

        private static Semaphore sph = new Semaphore(1, 1);

        private static bool isAdded = false;

        private static bool isCompleteded = false;

        static void Main(string[] args)
        {
            Console.WriteLine("5. Write a program which creates two threads and a shared collection:");
            Console.WriteLine("the first one should add 10 elements into the collection and the second should print all elements in the collection after each adding.");
            Console.WriteLine("Use Thread, ThreadPool or Task classes for thread creation and any kind of synchronization constructions.");
            Console.WriteLine();

            ThreadPool.QueueUserWorkItem(RunThreadForShowing);
            ThreadPool.QueueUserWorkItem(RunThreadFoAdding);

            Console.ReadLine();
        }

        private static void RunThreadForShowing(object state)
        {
            while (!isCompleteded)
            {
                if (isAdded)
                {
                    Show();
                    isAdded = false;
                }
            }
        }

        private static void RunThreadFoAdding(object state)
        {
            var newNumber = new int[] { 5, 7, 8, 9, 4, 5, 7, 7, 3, 1 };
            Add(newNumber);

            Thread.Sleep(200);

            var newNumber2 = new int[] { 1, 6, 5, 7, 9, 5, 7, 4, 3, 2 };
            Add(newNumber2);

            Thread.Sleep(1);
            isCompleteded = true;
        }

        public static void Add(int[] numbers)
        {
            sph.WaitOne();
            sharedCollection.AddRange(numbers);
            sph.Release();
            isAdded = true;
        }

        public static void Show()
        {
            sph.WaitOne();
            for (int i = 1; i <= sharedCollection.Count; i++)
            {
                Console.WriteLine($"[{string.Join(" ", sharedCollection.Take(i))}]");
                Thread.Sleep(10);
            }
            sph.Release();
        }

    }
}
