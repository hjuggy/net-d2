﻿/*
*  Create a Task and attach continuations to it according to the following criteria:
   a.    Continuation task should be executed regardless of the result of the parent task.
   b.    Continuation task should be executed when the parent task finished without success.
   c.    Continuation task should be executed when the parent task would be finished with fail and parent task thread should be reused for continuation
   d.    Continuation task should be executed outside of the thread pool when the parent task would be cancelled
   Demonstrate the work of the each case with console utility.
*/
using System;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreading.Task6.Continuation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Create a Task and attach continuations to it according to the following criteria:");
            Console.WriteLine("a.    Continuation task should be executed regardless of the result of the parent task.");
            Console.WriteLine("b.    Continuation task should be executed when the parent task finished without success.");
            Console.WriteLine("c.    Continuation task should be executed when the parent task would be finished with fail and parent task thread should be reused for continuation.");
            Console.WriteLine("d.    Continuation task should be executed outside of the thread pool when the parent task would be cancelled.");
            Console.WriteLine("Demonstrate the work of the each case with console utility.");
            Console.WriteLine();

            //var tokenSource = new CancellationTokenSource();
            //var token = tokenSource.Token;
            //SynchronizationContext.SetSynchronizationContext(new SynchronizationContext());
            //var context = TaskScheduler.FromCurrentSynchronizationContext();

            // a
            Task.Factory.StartNew(
                () =>
                {
                    Console.WriteLine("A: perent task run");
                    return;
                }, TaskCreationOptions.DenyChildAttach) 
                .ContinueWith(
                antecedent =>
                {
                    Console.WriteLine("A: Perent task complited. Now Continuation task run, regardless of the result of the parent task!");
                });


            Thread.Sleep(100);
            Console.WriteLine();
            // b

            Task.Factory.StartNew(
                () =>
                {
                    Console.WriteLine("B: perent task run");
                    try
                    {
                        throw new Exception();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                })
                .ContinueWith(
                antecedent =>
                {
                    Console.WriteLine("B: Perent task finished without success. Now Continuation task run!");
                }, TaskContinuationOptions.OnlyOnFaulted);


            Thread.Sleep(100);
            Console.WriteLine();

            //C 
            Task.Factory.StartNew(
                () =>
                {
                    Console.WriteLine("c: perent task run");
                    Console.WriteLine($"Thread id: {Thread.CurrentThread.ManagedThreadId}");
                    try
                    {
                        throw new Exception();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        throw;
                    }
                })
                .ContinueWith(
                antecedent =>
                {
                    Console.WriteLine("C: Perent task finished without success. Now Continuation task run!");
                    Console.WriteLine($"Thread id: {Thread.CurrentThread.ManagedThreadId}");
                },
                TaskContinuationOptions.OnlyOnFaulted | TaskContinuationOptions.ExecuteSynchronously);

            Thread.Sleep(100);
            Console.WriteLine();
            //d

            var cts = new CancellationTokenSource();
            CancellationToken token = cts.Token;
            cts.Cancel();

            var task = Task.FromCanceled(token);
            Task continuation =
                task.ContinueWith(
                    antecedent => Console.WriteLine("D: The continuation task is running."),
                    TaskContinuationOptions.OnlyOnCanceled);

            try
            {
                task.Wait();
                continuation.Wait();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"D: {ex.GetType().Name}: {ex.Message}");
                Console.WriteLine();
            }

            Console.WriteLine($"D: Task perent: {task.Status:G}");
            Console.WriteLine($"D: Task continuation: {continuation.Status:G}");

            Console.ReadLine();
        }

    }
}
