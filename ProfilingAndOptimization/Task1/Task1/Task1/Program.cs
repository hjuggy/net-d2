﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {
            var passwordText = "pasword";
            byte[] salt = Encoding.UTF8.GetBytes("dfklkllkdfgfvfvddxfv");

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            GeneratePasswordHashUsingSalt(passwordText, salt);
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds);// 82ms 

            stopwatch.Reset();
            stopwatch.Start();
            GeneratePasswordHashUsingSaltOptimized(passwordText, salt);//56ms
            stopwatch.Stop();
            Console.WriteLine(stopwatch.ElapsedMilliseconds);
            Console.ReadLine();
        }

        public static string GeneratePasswordHashUsingSalt(string passwordText, byte[] salt)
        {

            var iterate = 10000;
            var pbkdf2 = new Rfc2898DeriveBytes(passwordText, salt, iterate);
            byte[] hash = pbkdf2.GetBytes(36);

            byte[] hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            var passwordHash = Convert.ToBase64String(hashBytes);

            return passwordHash;
        }

        public static string GeneratePasswordHashUsingSaltOptimized(string passwordText, byte[] salt)
        {

            var iterate = 10000;
            var pbkdf2 = new Rfc2898DeriveBytes(passwordText, salt, iterate);
            byte[] hash = pbkdf2.GetBytes(20);

            var hashBytes = salt.Take(16).Concat(hash).ToArray();

            var passwordHash = Convert.ToBase64String(hashBytes);

            return passwordHash;
        }


    }
}
